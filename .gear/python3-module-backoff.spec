%define _unpackaged_files_terminate_build 1
%define pypi_name backoff

%def_with check

Name: python3-module-%pypi_name
Version: 2.2.1
Release: alt1
Summary: Function retry decorators with backoff strategies for synchronous and asynchronous code
License: MIT
Group: Development/Python3
Url: https://pypi.org/project/backoff
Vcs: https://github.com/litl/backoff.git
BuildArch: noarch

Source0: %name-%version.tar
Source1: %pyproject_deps_config_name

%pyproject_runtimedeps_metadata
BuildRequires(pre): rpm-build-pyproject
%pyproject_builddeps_build

%if_with check
%pyproject_builddeps_metadata
%pyproject_builddeps_check
%endif

%description
This package provides function decorators for retrying functions with
backoff strategies, useful for handling intermittent failures in
unreliable resources like network resources and external APIs. It
supports both synchronous functions and asyncio coroutines, making it
versatile for a wide range of applications. Decorators include options
for exponential, constant, and fibonacci backoff, as well as the ability
to specify conditions under which to give up retrying. Additionally,
it supports runtime configuration and custom event handlers for
flexible integration into various use cases.

%prep
%setup
%pyproject_deps_resync_check_poetry
%pyproject_deps_resync_build
%pyproject_deps_resync_metadata

%build
%pyproject_build

%install
%pyproject_install

%check
%pyproject_run_pytest

%files
%doc README.rst CHANGELOG.md LICENSE
%python3_sitelibdir/%pypi_name/
%python3_sitelibdir/%{pyproject_distinfo %pypi_name}

%changelog
* Sun Apr 7 2024 Aleksandr A. Voyt <sobue@altlinux.org> 2.2.1-alt1
- Initial build

